package main

import (
	"context"
	"fmt"
	"io/ioutil"
	"net/http"
	"regexp"
	"strings"
	"time"

	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/sirupsen/logrus"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	_ "k8s.io/client-go/plugin/pkg/client/auth/gcp"
)

type Detector struct {
	errorCodeFilter  string
	listenAddress    string
	namespaceFilter  string
	podLabelSelector string
	k8sClient        kubernetes.Interface
}

func (d *Detector) Serve() error {
	go func() {
		for {
			logrus.Info("Collecting report...")

			report, err := d.Run()
			if err != nil {
				logrus.WithError(err).Error("Failed to collect report")
			} else {
				logrus.Infof("Report was collected. Items found (correct/incorrect): %d/%d",
					report.TotalCorrect, report.TotalIncorrect)
				BadLogsStatusMetric.Reset()
				for _, item := range report.Items {
					if item.ErrorCode != "" {
						logrus.WithFields(logrus.Fields{
							"namespace":  item.Namespace,
							"name":       item.GetLabel("name"),
							"pod":        item.Pod,
							"container":  item.Container,
							"error_code": item.ErrorCode,
						}).Debugf("Bad logs found: %s", item.Line)
					}
					BadLogsStatusMetric.WithLabelValues(
						item.Namespace,
						item.GetLabel("name"),
						item.Container,
						item.ErrorCode,
					).Set(1)
				}
			}

			time.Sleep(time.Hour)
		}
	}()

	mux := http.NewServeMux()
	mux.Handle("/metrics", promhttp.Handler())
	mux.HandleFunc("/healthz", func(w http.ResponseWriter, r *http.Request) {
		_, _ = w.Write([]byte("ok"))
	})

	return http.ListenAndServe(d.listenAddress, mux)
}

func (d *Detector) Run() (*Report, error) {
	ctx := context.Background()
	report := &Report{
		ScanDate: time.Now(),
	}

	namespaceList, err := d.k8sClient.CoreV1().Namespaces().List(ctx, metav1.ListOptions{})
	if err != nil {
		return nil, err
	}

	namespaceFilterRE := regexp.MustCompile(d.namespaceFilter)
	errorCodeFilterRE := regexp.MustCompile(d.errorCodeFilter)
	for _, namespace := range namespaceList.Items {
		if !namespaceFilterRE.MatchString(namespace.Name) {
			continue
		}

		reportItems, err := d.processNamespace(namespace)
		if err != nil {
			return nil, err
		}

		if len(reportItems) == 0 {
			continue
		}

		for _, reportItem := range reportItems {
			if len(d.errorCodeFilter) > 0 && !errorCodeFilterRE.MatchString(reportItem.ErrorCode) {
				continue
			}
			if reportItem.ErrorCode == "" {
				report.TotalCorrect++
			} else {
				report.TotalIncorrect++
			}
			report.Items = append(report.Items, reportItem)
		}
	}

	return report, nil
}

func (d *Detector) processNamespace(namespace v1.Namespace) ([]*ReportItem, error) {
	var reportItems []*ReportItem
	ctx := context.Background()

	podList, err := d.k8sClient.CoreV1().Pods(namespace.Name).List(ctx, metav1.ListOptions{
		LabelSelector: d.podLabelSelector,
	})
	if err != nil {
		return nil, fmt.Errorf("failed to list pods in %s namespace", namespace.Name)
	}

	if len(podList.Items) == 0 {
		return nil, nil
	}

	for _, pod := range podList.Items {
		if len(pod.Spec.Containers) == 0 {
			continue
		}
		if pod.Status.Phase != v1.PodRunning {
			continue
		}
		var (
			container      v1.Container
			foundContainer bool
		)
		for _, c := range pod.Spec.Containers {
			if c.Name == "backend" || c.Name == "frontend" || c.Name == "service" {
				container = c
				foundContainer = true
				break
			}
		}

		if !foundContainer {
			continue
		}

		tailLines := int64(1)

		podLogs, err := d.k8sClient.CoreV1().Pods(namespace.Name).GetLogs(pod.Name, &v1.PodLogOptions{
			Container: container.Name,
			TailLines: &tailLines,
		}).Stream(ctx)
		if err != nil {
			return nil, fmt.Errorf("failed to get %s/%s logs", namespace.Name, pod.Name)
		}

		b, err := ioutil.ReadAll(podLogs)
		if err != nil {
			return nil, fmt.Errorf("failed to read %s/%s logs", namespace.Name, pod.Name)
		}

		reportItem := &ReportItem{
			Container: container.Name,
			Labels:    pod.Labels,
			Line:      strings.TrimSpace(string(b)),
			Namespace: namespace.Name,
			Pod:       pod.Name,
			Status:    string(pod.Status.Phase),
		}

		err = ValidateAsLogEntry(b)
		if err != nil {
			reportItem.Error = err.Error()
			reportItem.ErrorCode = CodeByError(err)
		}

		reportItems = append(reportItems, reportItem)
	}

	return reportItems, nil
}
