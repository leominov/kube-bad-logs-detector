package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes/fake"
)

func TestDetector_Run(t *testing.T) {
	k8sClient := fake.NewSimpleClientset(
		&v1.Namespace{
			ObjectMeta: metav1.ObjectMeta{
				Name: "app-namespace",
			},
		},
		&v1.Namespace{
			ObjectMeta: metav1.ObjectMeta{
				Name: "foobar",
			},
		},
		&v1.Pod{
			ObjectMeta: metav1.ObjectMeta{
				Name:      "pod-test-1",
				Namespace: "app-namespace",
			},
			Spec: v1.PodSpec{
				Containers: []v1.Container{
					{
						Name: "backend",
					},
				},
			},
			Status: v1.PodStatus{
				Phase: v1.PodRunning,
			},
		},
		&v1.Pod{
			ObjectMeta: metav1.ObjectMeta{
				Name:      "pod-test-2",
				Namespace: "app-namespace",
				Labels: map[string]string{
					"name": "tiller",
				},
			},
			Spec: v1.PodSpec{
				Containers: []v1.Container{
					{
						Name: "app",
					},
				},
			},
		},
		&v1.Pod{
			ObjectMeta: metav1.ObjectMeta{
				Name:      "pod-test-3",
				Namespace: "app-namespace",
			},
			Spec: v1.PodSpec{
				Containers: []v1.Container{
					{
						Name: "backend",
					},
				},
			},
		},
		&v1.Pod{
			ObjectMeta: metav1.ObjectMeta{
				Name:      "test-3",
				Namespace: "app-namespace",
			},
			Spec: v1.PodSpec{
				Containers: []v1.Container{
					{
						Name: "app",
					},
				},
			},
		},
		&v1.Pod{
			ObjectMeta: metav1.ObjectMeta{
				Name:      "pod-test",
				Namespace: "foobar",
			},
			Spec: v1.PodSpec{
				Containers: []v1.Container{
					{
						Name: "app",
					},
				},
			},
		},
	)
	d := &Detector{
		namespaceFilter:  "app",
		podLabelSelector: "name!=tiller",
		k8sClient:        k8sClient,
	}
	report, err := d.Run()
	if !assert.NoError(t, err) {
		return
	}
	assert.Equal(t, report.TotalCorrect+report.TotalIncorrect, len(report.Items))
	if assert.Equal(t, 1, report.TotalIncorrect) {
		item := report.Items[0]
		assert.Equal(t, "backend", item.Container)
		assert.Equal(t, "app-namespace", item.Namespace)
		assert.Equal(t, "fake logs", item.Line)
		assert.Equal(t, "incorrect_json", item.ErrorCode)
		assert.Equal(t, "pod-test-1", item.Pod)
		assert.Contains(t, item.Error, "invalid")
	}

	d = &Detector{
		errorCodeFilter:  "low_log_level",
		namespaceFilter:  "app",
		podLabelSelector: "name!=tiller",
		k8sClient:        k8sClient,
	}
	report, err = d.Run()
	if !assert.NoError(t, err) {
		return
	}
	assert.Equal(t, 0, report.TotalIncorrect)
	assert.Equal(t, 0, report.TotalCorrect)
}
