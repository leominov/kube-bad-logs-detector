package main

import "github.com/prometheus/client_golang/prometheus"

var (
	BadLogsStatusMetric = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: "kube_bad_logs_detector",
		Subsystem: "container",
		Name:      "info",
		Help:      "Information about container with bad logs.",
	}, []string{"namespace", "name", "container", "error_code"})
)

func init() {
	prometheus.MustRegister(
		BadLogsStatusMetric,
	)
}
