package main

import (
	"flag"
	"fmt"

	"github.com/sirupsen/logrus"
)

var (
	errorCodeFilter  = flag.String("error-code-filter", "", "Filter for errors")
	logLevel         = flag.String("log-level", "info", "Level of logging")
	namespaceFilter  = flag.String("namespace-filter", "app", "Filter for namespaces")
	podLabelSelector = flag.String("pod-label-selector", "name!=tiller", "Label selector for pods")
	reportFormat     = flag.String("report-format", "csv", "Report format (csv, json, yaml)")
	listenAddress    = flag.String("listen-address", "", "Address to serve HTTP requests")
)

func main() {
	flag.Parse()

	if level, err := logrus.ParseLevel(*logLevel); err == nil {
		logrus.SetLevel(level)
	}

	k8sClient, err := NewClientSet()
	if err != nil {
		logrus.Fatal(err)
	}

	detector := &Detector{
		errorCodeFilter:  *errorCodeFilter,
		listenAddress:    *listenAddress,
		namespaceFilter:  *namespaceFilter,
		podLabelSelector: *podLabelSelector,
		k8sClient:        k8sClient,
	}

	if *listenAddress == "" {
		report, err := detector.Run()
		if err != nil {
			logrus.Error(err)
		}
		fmt.Println(report.String(*reportFormat))
		return
	}

	logrus.SetFormatter(&logrus.JSONFormatter{})
	logrus.Fatal(detector.Serve())
}
