# kube-bad-logs-detector

* [![App Status](https://argo.infra.cloud.qlean.ru/api/badge?name=kube-bad-logs-detector-prod&revision=true)](https://argo.infra.cloud.qlean.ru/applications/kube-bad-logs-detector-prod) – infra

Поиск подов с логами не в JSON. Формат лога описан в файле `log_entry.go`.

## Запуск

```shell
Usage of ./kube-bad-logs-detector:
  -error-code-filter string
    	Filter for errors
  -listen-address string
    	Address to serve HTTP requests
  -log-level string
    	Level of logging (default "info")
  -namespace-filter string
    	Filter for namespaces (default "app")
  -pod-label-selector string
    	Label selector for pods (default "name!=tiller")
  -report-format string
    	Report format (csv, json, yaml) (default "csv")
```

## Пример отчета

```json
{
  "items": [
    {
      "container": "bot",
      "error": "incorrect JSON: invalid character '.' after top-level value",
      "error_code": "incorrect_json",
      "labels": {
        "branch": "master",
        "component": "bot",
        "env": "prod",
        "monitoring_scope": "app",
        "name": "changelog-bot",
        "pod-template-hash": "8668df47d4",
        "project": "platform",
        "version": "c858cfbcc7cb482da5cda1292acf51676a975077"
      },
      "line": "10.4.65.48 - - [06/Jul/2021 22:47:07] \"GET / HTTP/1.1\" 200 -",
      "namespace": "app-changelog-bot",
      "pod": "changelog-bot-8668df47d4-j95f4",
      "status": "Running"
    }
  ],
  "scan_date": "2021-07-07T08:41:40.504379+05:00",
  "total_items": 1
}
```

## Ссылки

* https://qleanru.atlassian.net/wiki/spaces/DOCS/pages/2006646953
