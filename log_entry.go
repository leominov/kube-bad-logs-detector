package main

import (
	"encoding/json"
)

type LogEntry struct {
	Level   string `json:"level"`
	Message string `json:"message"`
}

func ValidateAsLogEntry(b []byte) error {
	logEntry := LogEntry{}
	if err := json.Unmarshal(b, &logEntry); err != nil {
		return ErrIncorrectJSON{err}
	}
	if len(logEntry.Level) == 0 {
		return ErrEmptyLogLevel{}
	}
	if logEntry.IsDebug() {
		return ErrLowLogLevel{logEntry.Level}
	}
	return nil
}

func (l *LogEntry) IsDebug() bool {
	for _, level := range []string{
		"all",
		"debug",
		"notice",
		"trace",
		"verbose",
	} {
		if l.Level == level {
			return true
		}
	}
	return false
}
