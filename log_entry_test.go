package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestValidateAsLogEntry(t *testing.T) {
	tests := map[string]bool{
		"FOOBAR":                                  false,
		"{}":                                      false, // empty log level
		`{"level": 50}`:                           false,
		`{"level": "info"}`:                       true,
		`{"level": "info", "message": true}`:      false,
		`{"level": "info", "message": "true"}`:    true,
		`{"level": "debug", "message": "true"}`:   false,
		`{"level": "warning", "message": "true"}`: true,
	}
	for input, valid := range tests {
		assert.Equal(t, valid, ValidateAsLogEntry([]byte(input)) == nil)
	}
}
