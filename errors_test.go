package main

import (
	"errors"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCodeByError(t *testing.T) {
	tests := map[error]string{
		os.ErrClosed:         "unknown",
		errors.New("foobar"): "unknown",
		ErrLowLogLevel{}:     "low_log_level",
		ErrIncorrectJSON{}:   "incorrect_json",
		ErrEmptyLogLevel{}:   "empty_log_level",
	}
	for err, code := range tests {
		assert.Equal(t, code, CodeByError(err))
	}
}
