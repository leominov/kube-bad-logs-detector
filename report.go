package main

import (
	"encoding/json"
	"time"

	"github.com/gocarina/gocsv"
	"gopkg.in/yaml.v3"
)

type Report struct {
	Items          []*ReportItem `json:"items"`
	ScanDate       time.Time     `json:"scan_date"`
	TotalCorrect   int           `json:"total_correct"`
	TotalIncorrect int           `json:"total_incorrect"`
}

type ReportItem struct {
	Container string            `json:"container"`
	Error     string            `json:"error"`
	ErrorCode string            `json:"error_code"`
	Labels    map[string]string `json:"labels" csv:"-"`
	Line      string            `json:"line"`
	Namespace string            `json:"namespace"`
	Pod       string            `json:"pod"`
	Status    string            `json:"status"`
}

func (r *Report) String(format string) string {
	var b []byte
	switch format {
	case "yaml":
		b, _ = yaml.Marshal(r)
	case "json":
		b, _ = json.MarshalIndent(r, "", "  ")
	default: // csv
		b, _ = gocsv.MarshalBytes(r.Items)
	}
	return string(b)
}

func (r *ReportItem) GetLabel(name string) string {
	if v, ok := r.Labels[name]; ok {
		return v
	}
	return ""
}
