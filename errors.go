package main

import "fmt"

type ErrIncorrectJSON struct {
	Err error
}

func IsErrIncorrectJSON(err error) bool {
	_, ok := err.(ErrIncorrectJSON)
	return ok
}

func (e ErrIncorrectJSON) Error() string {
	return fmt.Sprintf("incorrect JSON: %s", e.Err.Error())
}

type ErrEmptyLogLevel struct{}

func IsErrEmptyLogLevel(err error) bool {
	_, ok := err.(ErrEmptyLogLevel)
	return ok
}

func (e ErrEmptyLogLevel) Error() string {
	return "log level must be specified"
}

type ErrLowLogLevel struct {
	Level string
}

func IsErrLowLogLevel(err error) bool {
	_, ok := err.(ErrLowLogLevel)
	return ok
}

func (e ErrLowLogLevel) Error() string {
	return fmt.Sprintf("log level is too low: %s", e.Level)
}

func CodeByError(err error) string {
	if IsErrLowLogLevel(err) {
		return "low_log_level"
	}
	if IsErrIncorrectJSON(err) {
		return "incorrect_json"
	}
	if IsErrEmptyLogLevel(err) {
		return "empty_log_level"
	}
	return "unknown"
}
